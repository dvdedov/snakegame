// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/TeleportBox.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTeleportBox() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ATeleportBox_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ATeleportBox();
	ENGINE_API UClass* Z_Construct_UClass_ATriggerBox();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ATeleportBox::execExitTeleporter)
	{
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ExitTeleporter(Z_Param_OverlappedActor,Z_Param_OtherActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATeleportBox::execEnterTeleporter)
	{
		P_GET_OBJECT(AActor,Z_Param_OverlappedActor);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EnterTeleporter(Z_Param_OverlappedActor,Z_Param_OtherActor);
		P_NATIVE_END;
	}
	void ATeleportBox::StaticRegisterNativesATeleportBox()
	{
		UClass* Class = ATeleportBox::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "EnterTeleporter", &ATeleportBox::execEnterTeleporter },
			{ "ExitTeleporter", &ATeleportBox::execExitTeleporter },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics
	{
		struct TeleportBox_eventEnterTeleporter_Parms
		{
			AActor* OverlappedActor;
			AActor* OtherActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::NewProp_OverlappedActor = { "OverlappedActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TeleportBox_eventEnterTeleporter_Parms, OverlappedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TeleportBox_eventEnterTeleporter_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::NewProp_OverlappedActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::NewProp_OtherActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TeleportBox.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATeleportBox, nullptr, "EnterTeleporter", nullptr, nullptr, sizeof(TeleportBox_eventEnterTeleporter_Parms), Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATeleportBox_EnterTeleporter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATeleportBox_EnterTeleporter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics
	{
		struct TeleportBox_eventExitTeleporter_Parms
		{
			AActor* OverlappedActor;
			AActor* OtherActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::NewProp_OverlappedActor = { "OverlappedActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TeleportBox_eventExitTeleporter_Parms, OverlappedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TeleportBox_eventExitTeleporter_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::NewProp_OverlappedActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::NewProp_OtherActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TeleportBox.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATeleportBox, nullptr, "ExitTeleporter", nullptr, nullptr, sizeof(TeleportBox_eventExitTeleporter_Parms), Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATeleportBox_ExitTeleporter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATeleportBox_ExitTeleporter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATeleportBox_NoRegister()
	{
		return ATeleportBox::StaticClass();
	}
	struct Z_Construct_UClass_ATeleportBox_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherTeleport_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherTeleport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_teleporting_MetaData[];
#endif
		static void NewProp_teleporting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_teleporting;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATeleportBox_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATriggerBox,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATeleportBox_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATeleportBox_EnterTeleporter, "EnterTeleporter" }, // 2216099409
		{ &Z_Construct_UFunction_ATeleportBox_ExitTeleporter, "ExitTeleporter" }, // 782422790
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATeleportBox_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "TeleportBox.h" },
		{ "ModuleRelativePath", "TeleportBox.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATeleportBox_Statics::NewProp_OtherTeleport_MetaData[] = {
		{ "Category", "Teleporter" },
		{ "ModuleRelativePath", "TeleportBox.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATeleportBox_Statics::NewProp_OtherTeleport = { "OtherTeleport", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATeleportBox, OtherTeleport), Z_Construct_UClass_ATeleportBox_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATeleportBox_Statics::NewProp_OtherTeleport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATeleportBox_Statics::NewProp_OtherTeleport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATeleportBox_Statics::NewProp_teleporting_MetaData[] = {
		{ "ModuleRelativePath", "TeleportBox.h" },
	};
#endif
	void Z_Construct_UClass_ATeleportBox_Statics::NewProp_teleporting_SetBit(void* Obj)
	{
		((ATeleportBox*)Obj)->teleporting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ATeleportBox_Statics::NewProp_teleporting = { "teleporting", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ATeleportBox), &Z_Construct_UClass_ATeleportBox_Statics::NewProp_teleporting_SetBit, METADATA_PARAMS(Z_Construct_UClass_ATeleportBox_Statics::NewProp_teleporting_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATeleportBox_Statics::NewProp_teleporting_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATeleportBox_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATeleportBox_Statics::NewProp_OtherTeleport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATeleportBox_Statics::NewProp_teleporting,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATeleportBox_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATeleportBox>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATeleportBox_Statics::ClassParams = {
		&ATeleportBox::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATeleportBox_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATeleportBox_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATeleportBox_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATeleportBox_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATeleportBox()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATeleportBox_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATeleportBox, 2737852295);
	template<> SNAKEGAME_API UClass* StaticClass<ATeleportBox>()
	{
		return ATeleportBox::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATeleportBox(Z_Construct_UClass_ATeleportBox, &ATeleportBox::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ATeleportBox"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATeleportBox);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
