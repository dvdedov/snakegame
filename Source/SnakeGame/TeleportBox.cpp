// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleportBox.h"

ATeleportBox::ATeleportBox()
{
	OnActorBeginOverlap.AddDynamic(this, &ATeleportBox::EnterTeleporter);
	OnActorEndOverlap.AddDynamic(this, &ATeleportBox::ExitTeleporter);
	teleporting = false;
}

void ATeleportBox::BeginPlay()
{
	Super::BeginPlay();
}

void ATeleportBox::EnterTeleporter(class AActor* OverlappedActor, class AActor* OtherActor)
{
	if (OtherActor && OtherActor != this)
	{
		if (OtherTeleport)
		{
			ASnakeElementBase* TPElement = Cast<ASnakeElementBase>(OtherActor);
			if (TPElement && !OtherTeleport->teleporting)
			{
				FVector TeleportingLocation(OtherTeleport->GetActorLocation());
				FVector ElementLocation(TPElement->GetActorLocation());
				TeleportingLocation.Y = ElementLocation.Y;
				teleporting = true;
				TPElement->SetActorLocation(TeleportingLocation);
			}
		}
	}
}

void  ATeleportBox::ExitTeleporter(class AActor* OverlappedActor, class AActor* OtherActor)
{
	if (OtherActor && OtherActor != this)
	{
		if (OtherTeleport && !teleporting)
		{
			OtherTeleport->teleporting = false;
		}
	}
}


//void ATeleportBox::EnterTeleporter(class AActor* OverlappedActor, class AActor* OtherActor)
//{
//	if (OtherActor && OtherActor != this)
//	{
//		if (OtherTeleport)
//		{
//			ASnakeElementBase* TPElement = Cast<ASnakeElementBase>(OtherActor);
//			if (TPElement && !OtherTeleport->teleporting)
//			{
//				teleporting = true;
//				TPElement->SetActorLocation(OtherTeleport->GetActorLocation());
//			}
//		}
//	}
//}
//
//void  ATeleportBox::ExitTeleporter(class AActor* OverlappedActor, class AActor* OtherActor)
//{
//	if (OtherActor && OtherActor != this)
//	{
//		if (OtherTeleport && !teleporting)
//		{
//			OtherTeleport->teleporting = false;
//		}
//	}
//}