// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "GameFramework/Actor.h"
#include "GameFramework/SpringArmComponent.h"
#include "SnakeElementBase.h"
#include "TeleportBox.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ATeleportBox : public ATriggerBox
{
	GENERATED_BODY()

public:
	ATeleportBox();

protected:
	virtual void BeginPlay() override;

public:
	
	UFUNCTION()
		void EnterTeleporter(class AActor* OverlappedActor, class AActor* OtherActor);

	UFUNCTION()
		void ExitTeleporter(class AActor* OverlappedActor, class AActor* OtherActor);

	UPROPERTY(EditAnywhere, Category = "Teleporter")
		ATeleportBox* OtherTeleport;

	UPROPERTY()
		bool teleporting;
};
