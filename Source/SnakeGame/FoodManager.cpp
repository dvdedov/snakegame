// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodManager.h"
#include "Food.h"

// Sets default values
AFoodManager::AFoodManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodManager::BeginPlay()
{
	Super::BeginPlay();
	SpawnFood();
}

// Called every frame
void AFoodManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodManager::SpawnFood()
{
	int rx = -450 + rand() % 900;
	int ry = -450 + rand() % 900;

	FVector NewFoodLocation(rx, ry, 0);
	FTransform FoodTransform(NewFoodLocation);
	AFood* NewSpawnedFood = GetWorld()->SpawnActor<AFood>(NewFood, FoodTransform);

	NewSpawnedFood->OnEatDone.AddDynamic(this, &AFoodManager::SpawnFood);
}