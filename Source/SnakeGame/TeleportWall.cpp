// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleportWall.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"

// Sets default values
ATeleportWall::ATeleportWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATeleportWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleportWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleportWall::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);

	if (IsValid(Snake))
	{
		auto Direction = Snake->LastMoveDirection;
		auto TPElement = Snake;
		//auto TPElement = Cast<ASnakeElementBase>(OverlappedComponent);
		FVector TPLocation = TPElement->GetActorLocation();
		int TPX = 450;
		int TPY = 450;
	
		switch (Direction)
		{
		case EMovementDirection::UP:

			TPLocation.X = -TPX;

			TPElement->SetActorLocation(TPLocation);
			break;
		case EMovementDirection::DOWN:
					
			TPLocation.X = TPX;

			TPElement->SetActorLocation(TPLocation);
			break;
		case EMovementDirection::LEFT:

			TPLocation.Y = -TPY;

			TPElement->SetActorLocation(TPLocation);
			break;
		case EMovementDirection::RIGHT:
	
			TPLocation.Y = TPY;

			TPElement->SetActorLocation(TPLocation);
			break;
		}
	}
}

