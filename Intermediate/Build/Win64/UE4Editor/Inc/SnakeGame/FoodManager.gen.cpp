// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/FoodManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFoodManager() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AFoodManager_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AFoodManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKEGAME_API UClass* Z_Construct_UClass_AFood_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AFoodManager::execSpawnFood)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnFood();
		P_NATIVE_END;
	}
	void AFoodManager::StaticRegisterNativesAFoodManager()
	{
		UClass* Class = AFoodManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SpawnFood", &AFoodManager::execSpawnFood },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AFoodManager_SpawnFood_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFoodManager_SpawnFood_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "FoodManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFoodManager_SpawnFood_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFoodManager, nullptr, "SpawnFood", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFoodManager_SpawnFood_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AFoodManager_SpawnFood_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFoodManager_SpawnFood()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFoodManager_SpawnFood_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AFoodManager_NoRegister()
	{
		return AFoodManager::StaticClass();
	}
	struct Z_Construct_UClass_AFoodManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewFood_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_NewFood;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFoodManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AFoodManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AFoodManager_SpawnFood, "SpawnFood" }, // 1717153388
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FoodManager.h" },
		{ "ModuleRelativePath", "FoodManager.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodManager_Statics::NewProp_NewFood_MetaData[] = {
		{ "Category", "FoodManager" },
		{ "ModuleRelativePath", "FoodManager.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AFoodManager_Statics::NewProp_NewFood = { "NewFood", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodManager, NewFood), Z_Construct_UClass_AFood_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AFoodManager_Statics::NewProp_NewFood_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodManager_Statics::NewProp_NewFood_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFoodManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodManager_Statics::NewProp_NewFood,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFoodManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFoodManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFoodManager_Statics::ClassParams = {
		&AFoodManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AFoodManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AFoodManager_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFoodManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFoodManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFoodManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFoodManager, 2899026231);
	template<> SNAKEGAME_API UClass* StaticClass<AFoodManager>()
	{
		return AFoodManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFoodManager(Z_Construct_UClass_AFoodManager, &AFoodManager::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AFoodManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFoodManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
