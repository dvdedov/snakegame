// Fill out your copyright notice in the Description page of Project Settings.


#include "BorderWall.h"
#include "SnakeBase.h"

// Sets default values
ABorderWall::ABorderWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABorderWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABorderWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABorderWall::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}

