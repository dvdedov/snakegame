// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_FoodManager_generated_h
#error "FoodManager.generated.h already included, missing '#pragma once' in FoodManager.h"
#endif
#define SNAKEGAME_FoodManager_generated_h

#define SnakeGame_Source_SnakeGame_FoodManager_h_14_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_FoodManager_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnFood);


#define SnakeGame_Source_SnakeGame_FoodManager_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnFood);


#define SnakeGame_Source_SnakeGame_FoodManager_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFoodManager(); \
	friend struct Z_Construct_UClass_AFoodManager_Statics; \
public: \
	DECLARE_CLASS(AFoodManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AFoodManager)


#define SnakeGame_Source_SnakeGame_FoodManager_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAFoodManager(); \
	friend struct Z_Construct_UClass_AFoodManager_Statics; \
public: \
	DECLARE_CLASS(AFoodManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AFoodManager)


#define SnakeGame_Source_SnakeGame_FoodManager_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFoodManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFoodManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodManager(AFoodManager&&); \
	NO_API AFoodManager(const AFoodManager&); \
public:


#define SnakeGame_Source_SnakeGame_FoodManager_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodManager(AFoodManager&&); \
	NO_API AFoodManager(const AFoodManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFoodManager)


#define SnakeGame_Source_SnakeGame_FoodManager_h_14_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_FoodManager_h_11_PROLOG
#define SnakeGame_Source_SnakeGame_FoodManager_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_INCLASS \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_FoodManager_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_FoodManager_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AFoodManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_FoodManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
